package jp.alhinc.iitsuka_masahiro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {


	public static void main(String[] args){
		//コマンドライン引数のチェック
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//支店情報用
		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();
		//商品用
		//支店コードの正規表現用
		String regex = "^[0-9]{3}$";
		//支店定義ファイル読み込み処理
		if ( !lstFileRead(args[0], "branch.lst", regex, "支店", branchNames, branchSales) ) {
			return;
		}

		//集計ファイル抽出
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for ( int i = 0; i < files.length ; i++ ) {
			//ファイル名称取得
			String fileName = files[i].getName();
			if( files[i].isFile() && fileName.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//連番チェック
		if (rcdFiles.size() >= 2) {
			Collections.sort(rcdFiles);
			int fileNameMin = Integer.parseInt(rcdFiles.get(0).getName().substring(0,8));
			int fileNameMax = Integer.parseInt(rcdFiles.get(rcdFiles.size() - 1).getName().substring(0,8));
			if( (fileNameMax - fileNameMin + 1) != rcdFiles.size()) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		BufferedReader br = null;
		for (int i = 0; i< rcdFiles.size(); i++) {
			try {
				ArrayList<String> list = new ArrayList<>();
				String fileName = rcdFiles.get(i).getName();
				String line = "";
				br = new BufferedReader(new FileReader(rcdFiles.get(i)));
				//rcdファイルの行毎の意味と行数
				int branchCodeLine = 0;
				int saleLine = 1;
				int lineSize = 2;
				long saleMax = 10000000000L;
				while ((line = br.readLine()) != null) {
					list.add(line);
				}
				//行数チェック
				if (list.size() != lineSize) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}
				//売上金額数字チェック
				if (!list.get(saleLine).matches("^[0-9]+$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//支店コードの存在確認
				if (!branchNames.containsKey(list.get(branchCodeLine))) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}
				String branchCode = list.get(branchCodeLine);
				// 支店足し算
				long fileSale = Long.parseLong(list.get(saleLine));
				Long saleAmount = branchSales.get(branchCode) + fileSale;
				if(saleAmount >= saleMax){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				branchSales.put(branchCode, saleAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//支店集計出力
		if (!outFileWrite(args[0], "branch.out", branchNames, branchSales)) {
			return;
		}

	}
	/**
	 *
	 * @param dirPath ディレクトリパス
	 * @param fileName ファイル名称
	 * @param regex 正規表現
	 * @param type  エラーメッセージで出力するときの名称
	 * @param names  コード、名前のマップ
	 * @param sales　コード、金額のマップ
	 * @return
	 */
	public static boolean lstFileRead(String dirPath, String fileName, String regex, String type, Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;
		try{
			File file = new File(dirPath, fileName);
			if(!file.exists()) {
				System.out.println(type + "定義ファイルが存在しません");
				return false;
			}
			br = new BufferedReader(new FileReader(file));
			String line;
			while((line = br.readLine()) != null){
				String[] items = line.split(",", -1);
				if((items.length != 2) || (!items[0].matches(regex))){
					System.out.println(type + "定義ファイルのフォーマットが不正です");
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if( br != null ){
					br.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}
		}
		return true;
	}
	/**
	 * 売上集計ファイル出力
	 * @param dirPath
	 * @param fileName
	 * @param names
	 * @param sales
	 * @return
	 */
	public static boolean outFileWrite(String dirPath, String fileName, Map<String, String> names, Map<String, Long> sales) {
		BufferedWriter bw = null;
		// ファイル出力
		try {
			File file = new File(dirPath, fileName);
			bw = new BufferedWriter( new FileWriter(file) );
			for (String key : sales.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			try {
				if( bw != null ){
					bw.close();
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return false;
			}

		}
		return true;
	}

}
